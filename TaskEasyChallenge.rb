require 'json'
require 'time'
require_relative 'person'

file = File.open "input.json"
data = JSON.parse(file.read)

@scheduleStart = Time.parse(data["Configuration"]["scheduleStart"])
@scheduleFinish= Time.parse(data["Configuration"]["scheduleFinish"])
@lunchStart = Time.parse(data["Configuration"]["lunchStart"])
@lunchFinish= Time.parse(data["Configuration"]["lunchFinish"])

@employees = []
meetings = data["Workers"]

def createMeetings(meetings)
	meetings.each do |worker|
		person = Person.new(worker["name"])	
		person.addMeetings(worker["meeting"])
		@employees.push(person)
	end
end

def isBusy(employee,time)
	result = false
	employee.getMeetings.each do |hour|
		if @scheduleStart==Time.parse(hour)
			result=true
		end
	end
	return result
end

def writeJSONResults(text)
	@jsonString+=text
end

#Start Program
@jsonString=""
createMeetings(meetings)
writeJSONResults("{\"schedule\":[")

while @scheduleStart<@scheduleFinish do	
	if @scheduleStart<@lunchStart or @scheduleStart>=@lunchFinish		
		flag=0
		freeEmployees=[]
		@employees.each do |employee|			
			if !isBusy(employee,@scheduleStart)
				flag+=1
				freeEmployees.push(employee)
			end
		end
		if flag>=3			
			writeJSONResults("{\"hour\":\"#{@scheduleStart}\",\"Employees\":[")
			last = freeEmployees.last
			freeEmployees.each do |free|
				if last==free					
					writeJSONResults("\"#{free.name}\"")
				else					
					writeJSONResults("\"#{free.name}\",")
				end				
			end

			if (@scheduleStart+1800)>=@scheduleFinish				
				writeJSONResults("]}")
			else
				writeJSONResults("]},")				
			end
			
		end
			
	end	
	@scheduleStart+=1800	
end

writeJSONResults("]}")


File.write('results.json', @jsonString)
puts "JSON file with results successfully written"



