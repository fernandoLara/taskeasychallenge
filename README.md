# taskEasyChallenge

This program is written in ruby. Initially it reads the file 'input.json', which contains the configuration and input data for the program.

When the execution of the program finishes correctly, a json file named 'results.json' is delivered as a result.This contains the hours in which at least three people have no meeting

To execute the program it is necessary to write in the command line '> ruby TaskEasyChallenge.rb'

# Unit Test

You can found a single and basic example of unit test in the file 'person-test.rb'. You can run the test using the command: '> ruby person-test.rb'



