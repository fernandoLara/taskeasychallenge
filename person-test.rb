require 'test/unit'
require_relative  'person'
require_relative  'TaskEasyChallenge'

class TestPersonClass < Test::Unit::TestCase
	def test_atributes
		person = Person.new("QA")
		assert_match("QA", person.name )
		assert_no_match(/\d/, person.name )
		
		person.addMeetings("[\"7:00 AM\",\"9:00 AM\",\"3:30 PM\"]")
		assert_match("[\"7:00 AM\",\"9:00 AM\",\"3:30 PM\"]",person.getMeetings)		
	end
	
end